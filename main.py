#!/usr/bin/env python3

from sqlite3 import connect

from fastapi import FastAPI, HTTPException
from fastapi.middleware.cors import CORSMiddleware


def open_db():
    con = connect("mass.db")
    cur = con.cursor()
    return con, cur


with open("key", "r") as f:
    KEY = f.read().strip()

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=[
        "*",  # TODO
    ],
    allow_methods=["GET", "POST"],
)


@app.get("/")
def root():
    return "Masses API"


@app.get("/list_masses")
def list_masses(key: str):
    if key != KEY:
        raise HTTPException(401)

    con, cur = open_db()

    masses = cur.execute("SELECT time, value FROM mass").fetchall()

    con.close()

    return masses


@app.post("/add")
def add(key: str, mass: float):
    if key != KEY:
        raise HTTPException(401)

    con, cur = open_db()

    cur.execute("INSERT INTO mass (value) VALUES (?)", (mass,))

    con.commit()

    con.close()
